
var myapp = {
    data() {
        return {
            menuDataUrl: menuDataUrl,
            siteDataUrl: siteDataUrl,
            siteDataModUrl:siteDataModUrl,
            siteDetails: siteId,
            menuDatas: null,
            siteData: null,
            siteDataMod: null,
        }
    },
    methods: {
        getMenuDetails() {
            try {
                axios.post(this.menuDataUrl, this.siteDetails).then((response) => {
                    this.menuDatas = response.data.data;
                    var n1n1 = response.data.data;
                });
            } catch (e) {
                console.log(e);
            }
        },
        getSiteDetails() {
            try {
                axios.post(this.siteDataUrl, this.siteDetails).then((response) => {
                    this.siteData = response.data.data[0];
                });
            } catch (e) {
                console.log(e);
            }
        },
        getSiteDetailsMod() {
            try {
                console.log(this.siteDataModUrl);
                axios.get(this.siteDataModUrl).then((response) => {
                    this.siteDataMod = response.data.data[0];
                    window.doSomethingWithA(response.data.data[0]);
                    this.loadslider();
//                    yourGlobal.data = response.data.data[0];
                    // window.doSomethingWithA(response.data.data[0]);
                    // this.loadslider();
                });
            } catch (e) {
                console.log(e);
            }
        },
        loadslider() {
            $('#slides').superslides({
                inherit_width_from: '.cover-slides',
                inherit_height_from: '.cover-slides',
                play: 5000,
                animation: 'fade',
            }, 2000);
        },
        timeConvert(time) {
            if(time){
                // Check correct time format and split into components
                time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

                if (time.length > 1) { // If time format correct
                    time = time.slice(1);  // Remove full string match value
                    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
                    time[0] = +time[0] % 12 || 12; // Adjust hours
                }
                return time.join('');
            } else {
                return '';
             // return adjusted time or original string
            }
        },
        dayName(day) {
            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            return days[day];
          },
    },
    // watch: {
    //   resource: {
    //     immediate: true,
    //     handler() {
    //       this.getDetails();
    //     },
    //   },
    // },
    beforeMount() {
        this.getSiteDetails();
        this.getSiteDetailsMod();
    },
};
Vue.createApp(myapp).mount('#rp-app');